import threading
import iPhoneBase
import iPhoneGEOLocation
import time
import NodeInformation
import PushNotification
import logging





class iPhoneCheck(threading.Thread):

    def run(self):
        threading.current_thread().name='Thread - Check iPhone Device Location'
        self.CheckDeviceLocation()


    def CheckDeviceLocation(self):

        log = logging.getLogger('iPhoneCheckNofication')




        DeviceHomeNotification_InZone = 1
        DeviceHomeNotification_OutZone = 1

        DeviceOfficeNotification_InZone = 1
        DeviceOfficeNotification_OutZone = 1

        error = True


        while True:

            try:
                if error:
                    log.debug ("Creating connection to Apple ....")

                    iCloudReference = iPhoneBase.PyiCloudService(NodeInformation.itunesUser,NodeInformation.itunesPass)

                    log.debug ("Creating connection to Apple Compeleted")
            except:
                error = True
                log.debug("Connection not created")



            try:
                log.debug("Checking Status")

                located =  iCloudReference.findiphone(0).location()


                log.debug ("****************************** ALL DEVICES *********************************")
                log.debug (iCloudReference.devices)
                log.debug ("****************************************************************************")


                log.debug ("Device Latitude : " + str(located['latitude']))
                log.debug ("Device Longitude : " + str(located['longitude']))




                #result = gpsOffice(located['latitude'], located['longitude'])


                if iPhoneGEOLocation.CheckDeviceLocation_Home(located['latitude'], located['longitude']):
                    log.debug ("*** DEVICE IS IN ZONE - HOME ***")

                    if DeviceHomeNotification_InZone == 1:
                        PushNotification.SendPushOver("*** DEVICE IS IN ZONE - HOME ***")
                        DeviceHomeNotification_InZone = 0
                        DeviceHomeNotification_OutZone = 1


                else:
                    print ("*** DEVICE OUTSIDE ZONE - HOME ***")
                    if DeviceHomeNotification_OutZone == 1:
                        PushNotification.SendPushOver("*** DEVICE OUTSIDE ZONE - HOME ***")
                        DeviceHomeNotification_OutZone = 0
                        DeviceHomeNotification_InZone = 1



                if iPhoneGEOLocation.CheckDeviceLocation_Office(located['latitude'], located['longitude']):
                    print ("*** DEVICE IS IN ZONE - OFFICE ***")
                    if DeviceOfficeNotification_InZone == 1:
                        PushNotification.SendPushOver("*** DEVICE IS IN ZONE - OFFICE ***")
                        DeviceOfficeNotification_InZone = 0
                        DeviceOfficeNotification_OutZone = 1


                else:
                    print ("*** DEVICE OUTSIDE ZONE - OFFICE ***")
                    if DeviceOfficeNotification_OutZone == 1:
                        PushNotification.SendPushOver("*** DEVICE OUTSIDE ZONE - OFFICE ***")
                        DeviceOfficeNotification_OutZone = 0
                        DeviceOfficeNotification_InZone = 1

                error = False

            except Exception as e:

                log.debug (e)

                error = True
                log.debug ("Error collecting data")


            log.debug ("Sleeping Thread")
            time.sleep(600)



#test = iPhoneCheck()
#test.CheckDeviceLocation()













