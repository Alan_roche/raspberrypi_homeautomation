import NodeInformation
import threading
import MySQLdb
import time
import SpeakText
import logging
import os
import AdaptiveTextEngine



class CheckNotifications(threading.Thread):

    def run(self):

        threading.current_thread().name='Thread - Notification Checking'
        print NodeInformation.OKGREEN + "\nThread Name : " + threading.currentThread().getName() + "\n" + NodeInformation.OKWHITE
        self.CheckNotifcations()


    def getSQLConnection(self):

        db = MySQLdb.connect(host=NodeInformation.sql_address, # your host, usually localhost
                        user=NodeInformation.sql_user, # your username
                        passwd=NodeInformation.sql_pass, # your password
                        db=NodeInformation.sql_database) # name of the data base
        return db

    def getCheckDeviceOnline(self , inputUser):

        db = self.getSQLConnection()

        cur = db.cursor()

        cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Status = '1' and Device_Current_Location = %s and Device_Name = %s", (NodeInformation.getNodeName(),inputUser,))

        for row in cur.fetchall() :
            if row == None:
                print "getCheckDeviceOnly NO DEVICE ONLINE NOW : Return FALSE : " , inputUser
                db.close()
                return False
            else:
                print "getCheckDeviceOnly DEVICE STILL ONLINE: Return TRUE : " , inputUser
                db.close()
                return True
    def setDeviceNotification(self,Devicename , DeviceBSD , Status):
        db = self.getSQLConnection()

        cur = db.cursor()

        cur.execute("update Global_Device_Table SET Device_Notification=%s , Device_Current_Location = %s WHERE Device_Name = %s" ,(Status,NodeInformation.getNodeName(), Devicename))

        # Commit Database Changes
        db.commit()

        db.close()

    def getRSSIMaxMin(self, CurrentRSSI):
        log = logging.getLogger('getRSSIMaxMin')

        log.debug("Convert MAX MIN to DB access")

        rssiMAX = -10
        rssiMin = 0

        if rssiMAX <= CurrentRSSI and rssiMin >= CurrentRSSI:
            log.debug("RSSI in SCOPE : %s" , CurrentRSSI)
            return True
        else:
            log.debug ("RSSI out of SCOPE : %s " , CurrentRSSI)
            return False

    def playWelcomeTone(self , PlayTone):
        try:
            if PlayTone == 1:
                value = os.system("mpg123 -q NewMessage.mp3")
                if not value == 0:
                    raise Exception

        except:
            print NodeInformation.FAIL + "\n ERROR : No mpg123 installed\n" + NodeInformation.OKWHITE

    def getTotalMessageCount(self,Playtone,Node , Type):
        db = self.getSQLConnection()

        cur = db.cursor()

        cur.execute("SELECT * FROM Messages_Global WHERE User = %s and isPrivate = %s", (Node,Type,))

        TotalMessages = len(cur.fetchall())


        if Type == 1:
            Private = "Private"
        else:
            Private = "Public"


        if Playtone == 1:
            if TotalMessages == 1:
                SpeakText.GoogleSpeakText("You have received %s %s message" %(TotalMessages,Private))
            else:
                SpeakText.GoogleSpeakText("You have received %s %s messages" %(TotalMessages,Private))






    def CheckNotifcations(self):

        log = logging.getLogger('CheckNotifications')

        log.debug("CheckNotifcations Starting")

        Running = True

        while Running:
            try:
                print NodeInformation.OKGREEN + "*********** Checking for Online Devices and new Notifications ***********" + NodeInformation.OKWHITE

                log.debug("Reseting Global Welcome Tone Message")
                GlobalWelcomeTonePrivate = 1
                GlobalWelcomeTonePublic = 1

                log.debug("Checking for Online Devices and new Notifications")
                db = self.getSQLConnection()

                cur = db.cursor()

                cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Status = '1' and Device_Current_Location = %s", (NodeInformation.getNodeName(),))

            # print all the first cell of all the rows
                for row in cur.fetchall():
                    print "Notifcations Found Device Online : ",  row[0]

                    log.debug("Found Device Online :%s ", row[0])


                    log.debug("Checking RSSI Current Value")

                    RSSIinScope = self.getRSSIMaxMin(row[7])

                    log.debug("Checking RSSI scope DONE")

                    log.debug("Checking for Welcome Message")

                    if row[6] == 1 and RSSIinScope :
                        SpeakText.GoogleSpeakText("Good %s %s " % (AdaptiveTextEngine.getMorningAfternoonEvening(), row[2]))
                        self.setDeviceNotification(row[0] , row[1] , 0)
                        log.debug("Good %s %s " % (AdaptiveTextEngine.getMorningAfternoonEvening(), row[2]))


                    #if not row == None:
                    print "Collecting New Messages for : ", row[0]
                    log.debug("Collecting New Messages for :%s ", row[0])

                    cur.execute("SELECT * FROM Messages_Global WHERE User = %s and isPrivate = 1", (row[0],))

                    for rowMessage in cur.fetchall():

                        if self.getCheckDeviceOnline(row[0]) and RSSIinScope:

                            self.playWelcomeTone(GlobalWelcomeTonePrivate)
                            self.getTotalMessageCount(GlobalWelcomeTonePrivate,row[0],1)
                            GlobalWelcomeTonePrivate = 1

                            print "DateTime : " , rowMessage[0]
                            print "Subject : " , rowMessage[1]
                            print "User : "  , row[0]

                            #SpeakText.GoogleSpeakText("Message Received at %s %s." %(rowMessage[0],rowMessage[1]))

                            SpeakText.GoogleSpeakText("Message Received at %s %s. %s." %(AdaptiveTextEngine.getSimpleDate(rowMessage[0]),AdaptiveTextEngine.getSimpleTime(rowMessage[0]) ,rowMessage[1]))

                            cur.execute("DELETE FROM Messages_Global WHERE DateTime = %s and Subject = %s and User = %s and isPrivate = 1",(rowMessage[0],rowMessage[1],row[0]))
                                        #DELETE FROM `Messages_Global` WHERE `DateTime` = '2015-05-24 18:07:49' and `Subject` = 'Test' and `User` = 'Alan Roche' and `isPrivate` = '1'
                            db.commit()


                    cur.execute("SELECT * FROM Messages_Global WHERE User = %s and isPrivate = 0", (row[0],))

                    for rowMessage in cur.fetchall():

                        if self.getCheckDeviceOnline(row[0]) and RSSIinScope:

                            self.playWelcomeTone(GlobalWelcomeTonePublic)
                            self.getTotalMessageCount(GlobalWelcomeTonePublic,row[0],0)
                            GlobalWelcomeTonePublic = 0

                            print "DateTime : " , rowMessage[0]
                            print "Subject : " , rowMessage[1]
                            print "User : "  , row[0]

                            #SpeakText.GoogleSpeakText("Message Received at %s %s" %(rowMessage[0],rowMessage[1]))

                            SpeakText.GoogleSpeakText("Message Received at %s %s. %s." %(AdaptiveTextEngine.getSimpleDate(rowMessage[0]),AdaptiveTextEngine.getSimpleTime(rowMessage[0]),rowMessage[1]))


                            print "Remove Message from Database : ", rowMessage[1]

                            cur.execute("DELETE FROM Messages_Global WHERE DateTime = %s and Subject = %s and User = %s and isPrivate = 0",(rowMessage[0],rowMessage[1],row[0]))
                                        #DELETE FROM `Messages_Global` WHERE `DateTime` = '2015-05-24 18:07:49' and `Subject` = 'Test' and `User` = 'Alan Roche' and `isPrivate` = '1'
                            db.commit()

                try:
                    log.debug("Closing Database connection")
                    db.close()
                except:
                    log.debug("Error closing Database connection")

            except:
                print "Exception Occured in the Notifications Module "

            log.debug("CheckNotifcations Sleeping")
            print NodeInformation.OKGREEN + "*********** Finished Checking for Online Devices and new Notifications ***********" + NodeInformation.OKWHITE
            print ("Sleeping CheckNotifications")
            time.sleep(5)







