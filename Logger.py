
import logging
from logging import handlers, Formatter




def startLogger():



    '''logging.basicConfig(filename='HomeAutomation.log',
                    level=logging.DEBUG,maxBytes=1, backupCount=5,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log = logging.getLogger('Logger')
    log.debug("Logging Enabled Started")
    '''
    logger = logging.getLogger()
    Log_FileName='./logs/HomeAutomation.log'
    #Add Stdout logging
    #logging.getLogger().addHandler(logging.StreamHandler())

    consoleLogger = logging.StreamHandler()


    handler = logging.handlers.RotatingFileHandler(Log_FileName,maxBytes=50000000,backupCount=4)

    log_format = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logger.setLevel(logging.DEBUG)

    handler.setFormatter(log_format)
    consoleLogger.setFormatter(log_format)
    logger.addHandler(consoleLogger)
    logger.addHandler(handler)





