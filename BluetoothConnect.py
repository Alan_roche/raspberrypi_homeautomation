import subprocess
import time
import bluetooth
import NodeInformation
import fcntl
import struct
import array
import bluetooth
import bluetooth._bluetooth as bt


def poll_device(addr , bluetooth_device_name):

    global_status = False
    notification = True
    not_found_counter = 0
    maxretrycount = 2


    print NodeInformation.OKGREEN + "----------Check Device ONLINE / OFFLINE-----------\n" + NodeInformation.OKWHITE
    running = True

    while running:

        time.sleep(2)

        found = what_services(addr, bluetooth_device_name)

        if not found and not not_found_counter > maxretrycount:
            not_found_counter = not_found_counter+1
            print NodeInformation.WARNING + "Device %s going Offline : %s : Retry %s" % (bluetooth_device_name,addr,not_found_counter) + NodeInformation.OKWHITE

        if found:
            not_found_counter = 0
            print NodeInformation.OKBLUE + "Status device %s : Online : %s" % (bluetooth_device_name,addr) + NodeInformation.OKWHITE
            Device_RSSI = bluetooth_rssi(addr)
            running = False
            global_status = True
            print  NodeInformation.OKGREEN + "\n-----------------DONE----------------------\n" + NodeInformation.OKWHITE
            return True , Device_RSSI



        if not found and not_found_counter > maxretrycount:
            print NodeInformation.FAIL + "Status device %s : Offline : %s" % (bluetooth_device_name,addr) + NodeInformation.OKWHITE
            running = False
            notification = True
            global_status = False
            Device_RSSI = -255
            print NodeInformation.OKGREEN + "\n-----------------DONE----------------------\n" + NodeInformation.OKWHITE
            return False , Device_RSSI




        '''if global_status and notification:
            subprocess.call(["espeak", "device online"])
            notification = False
        '''




def bluetooth_rssi(addr):
    # Open hci socket
    hci_sock = bt.hci_open_dev()
    hci_fd = hci_sock.fileno()

    # Connect to device (to whatever you like)
    bt_sock = bluetooth.BluetoothSocket(bluetooth.L2CAP)
    bt_sock.settimeout(10)
    result = bt_sock.connect_ex((addr, 1))	# PSM 1 - Service Discovery

    try:
        # Get ConnInfo
        reqstr = struct.pack("6sB17s", bt.str2ba(addr), bt.ACL_LINK, "\0" * 17)
        request = array.array("c", reqstr )
        handle = fcntl.ioctl(hci_fd, bt.HCIGETCONNINFO, request, 1)
        handle = struct.unpack("8xH14x", request.tostring())[0]

        # Get RSSI
        cmd_pkt=struct.pack('H', handle)
        rssi = bt.hci_send_req(hci_sock, bt.OGF_STATUS_PARAM,
                     bt.OCF_READ_RSSI, bt.EVT_CMD_COMPLETE, 4, cmd_pkt)
        rssi = struct.unpack('b', rssi[3])[0]

        # Close sockets
        bt_sock.close()
        hci_sock.close()

        return rssi

    except:
        return None








def what_services(addr, bluetooth_device_name):
    try:
        discovered_device_name = bluetooth.lookup_name(addr,2)

        device_address = addr

        if discovered_device_name == bluetooth_device_name:
            #print "Device %s found with address %s" % (bluetooth_device_name, device_address)
            '''for services in bluetooth.find_service(address = addr):

                print "\t Name:           %s" % (services["name"])
                print "\t Description:    %s" % (services["description"])
                print "\t Protocol:       %s" % (services["protocol"])
                print "\t Provider:       %s" % (services["provider"])
                print "\t Port:           %s" % (services["port"])
                print "\t service-classes %s" % (services["service-classes"])
                print "\t profiles        %s" % (services["profiles"])
                print "\t Service id:  %s" % (services["service-id"])
                print ""'''
            return True
        else:
            #print "Device %s not found with address %s" % (bluetooth_device_name, device_address)
            return False

    except:
        pass
        #print "Device %s not found with address %s" % (bluetooth_device_name, device_address)
        return False






