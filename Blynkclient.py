import select, socket, struct
import os, sys, time, getopt
import MySQLdb , NodeInformation ,os , logging
from threading import Thread





# Default options
SERVER = "cloud.blynk.cc"
PORT = 8442
NODELAY = 1     # TCP_NODELAY
SNDBUF = 0      # No SNDBUF override
RCVBUF = 0      # No RCVBUF override
TOKEN = "7b9abdb3332f4ba580afa075eceae3d5"
DUMP = 0




# Blynk protocol helpers

hdr = struct.Struct("!BHH")

class MsgType:
    RSP    = 0
    LOGIN  = 2
    PING   = 6
    BRIDGE = 15
    HW     = 20

class MsgStatus:
    OK     = 200

def hw(*args):
    # Convert params to string and join using \0
    data = "\0".join(map(str, args))
    dump("< " + " ".join(map(str, args)))
    # Prepend HW command header
    return hdr.pack(MsgType.HW, genMsgId(), len(data)) + data


def handle_hw(data,conn):

    log = logging.getLogger('BlynkClient.handle_hw')

    params = data.split("\0")
    cmd = params.pop(0)
    if cmd == 'info':
        pass

    ### DIRECT pin operations
    elif cmd == 'pm':
        pairs = zip(params[0::2], params[1::2])
        for (pin, mode) in pairs:
            pin = int(pin)
            if mode == 'in':
                log.debug("Pin %d mode INPUT" % pin)
            elif mode == 'out':
                log.debug("Pin %d mode OUTPUT" % pin)
            elif mode == 'pu':
                log.debug("Pin %d mode INPUT_PULLUP" % pin)
            elif mode == 'pd':
                log.debug("Pin %d mode INPUT_PULLDOWN" % pin)
            else:
                log.debug("Unknown pin %d mode: %s" % (pin, mode))

    elif cmd == 'dw':
        pin = int(params.pop(0))
        val = params.pop(0)
        log.debug("Digital write pin %d, value %s" % (pin, val))
        
    elif cmd == 'aw':
        pin = int(params.pop(0))
        val = params.pop(0)
        log.debug("Analog write pin %d, value %s" % (pin, val))
        
    elif cmd == 'dr':           # This should read digital pin
        pin = int(params.pop(0))
        log.debug("Digital read pin %d" % pin)
        conn.sendall(hw("dw", pin, 1)) # Send value

    elif cmd == 'ar':           # This should do ADC read
        pin = int(params.pop(0))
        log.debug("Analog read pin %d" % pin)
        conn.sendall(hw("aw", pin, 123)) # Send value

    ### VIRTUAL pin operations
    elif cmd == 'vw':           # This should call user handler
        pin = int(params.pop(0))
        val = params.pop(0)
        log.debug("Virtual write pin %d, value %s" % (pin, val))






        if pin == 50:
            processCommands(conn ,pin)

        
    elif cmd == 'vr':           # This should call user handler
        pin = int(params.pop(0))
        log.debug("Virtual read pin %d" % pin)

        if pin == 0:
            updateTotalNotifications(conn,pin)
            log.debug("Virtual PIN 0 Found updating email status")

        if pin == 1:
            getTotalClientsOnline(conn,pin)

        if pin == 2:
            getTotalClientsOffline(conn,pin)

        #conn.sendall(hw("vw", pin, "hello")) # Send value
        
    else:
        log.debug("Unknown HW cmd: %s" % cmd)

static_msg_id = 1
def genMsgId():
    global static_msg_id
    static_msg_id += 1
    return static_msg_id

# Other utilities

start_time = time.time()
def log(msg):
    pass
    #print "[{:7.3f}] {:}".format(float(time.time() - start_time), msg)

def dump(msg):
    if DUMP:
        log(msg)

def receive(sock, length):
    log = logging.getLogger('BlynkClient.receive')
    log.debug("Running receive")
    d = []
    l = 0

    timeout = 0

    while l < length and getonLinkCheck() and timeout < 50:
        timeout = timeout+1

        log.debug("Running while loop : %s ",timeout)

        r = ''
        try:
            r = sock.recv(length-l)
        except socket.timeout:
            continue
        if not r:
            return ''
        d.append(r)
        l += len(r)
    log.debug("Finished receive")
    return ''.join(d)

# Threads

def readthread(conn):

    log = logging.getLogger('BlynkClient.readthread')

    while (getonLinkCheck()):
        log.debug("readthread")
        data = receive(conn, hdr.size)
        if not data:
            break
        msg_type, msg_id, msg_len = hdr.unpack(data)
        dump("Got {0}, {1}, {2}".format(msg_type, msg_id, msg_len))
        if msg_type == MsgType.RSP:
            pass
        elif msg_type == MsgType.PING:
            log.debug("Got ping")
            # Send Pong
            conn.sendall(hdr.pack(MsgType.RSP, msg_id, MsgStatus.OK))
        elif msg_type == MsgType.HW or msg_type == MsgType.BRIDGE:
            data = receive(conn, msg_len)
            # Print HW message
            dump("> " + " ".join(data.split("\0")))
            handle_hw(data ,conn)
        else:
            log.debug("Unknown msg type")
            break

    log.debug("Closing Thread")


def writethread(conn):

    log = logging.getLogger('BlynkClient.writethread')
    log.debug("Running writeThread")

    while (getonLinkCheck()):

        log.debug("Sending heartbeat...")
        conn.sendall(hdr.pack(MsgType.PING, genMsgId(), 0))
        time.sleep(10)

    log.debug("Closing Thread")





def getSQLConnection():

        db = MySQLdb.connect(host=NodeInformation.sql_address, # your host, usually localhost
                        user=NodeInformation.sql_user, # your username
                        passwd=NodeInformation.sql_pass, # your password
                        db=NodeInformation.sql_database) # name of the data base
        return db






def updateTotalNotifications(conn ,pin):
    db = getSQLConnection()
    cur = db.cursor()
    cur.execute("SELECT * FROM Messages_Global")
    sqlMsgCount = len(cur.fetchall())
    conn.sendall(hw("vw", pin, sqlMsgCount))

def getTotalClientsOnline(conn,pin):
    db = getSQLConnection()
    cur = db.cursor()
    cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Status = '1' and Device_Current_Location = %s", (NodeInformation.getNodeName(),))
    sqlOnlineClients = len(cur.fetchall())
    conn.sendall(hw("vw", pin, sqlOnlineClients))


def getTotalClientsOffline(conn, pin):
    db = getSQLConnection()
    cur = db.cursor()
    cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Status = '0' and Device_Current_Location = %s", (NodeInformation.getNodeName(),))
    sqlOfflineClients = len(cur.fetchall())
    conn.sendall(hw("vw", pin, sqlOfflineClients))




def processCommands(conn,pin):
    log = logging.getLogger('BlynkClient.processComands')
    log.debug("Process Command DEF")
    if pin == 50:
        log.debug("Running Reboot")
        os.system("reboot")


def pushMessages(conn):
    log = logging.getLogger('BlynkClient.pushMessages')
    while (getonLinkCheck()):
        log.debug("pushMessages to Cloud")
        conn.sendall(hw("vr", 30, 1))
        time.sleep(5)

    log.debug("Closing Thread")



def getonLinkCheck():

    log = logging.getLogger('BlynkClient.onLinkCheck')

    log.debug("onLinkCheck")

    try:
        conn1 = socket.create_connection((SERVER, PORT), 3)
        result = 1
    except:
        result = 0

    if (result == 1):
        log.debug("Server ONLINE")
        return True
    else:
        log.debug("Server OFFLINE")
        return False




def runMain():

    Connected = True

# Main code
    log = logging.getLogger('BlynkClient')

    log.debug('Connecting to %s:%d' % (SERVER, PORT))
    try:
        conn = socket.create_connection((SERVER, PORT), 3)
    except:
        log.debug("Can't connect")
        #sys.exit(1)
        return

    if NODELAY != 0:
        conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    if SNDBUF != 0:
        sndbuf = conn.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF)
        log.debug('Default SNDBUF %s changed to %s' % (sndbuf, SNDBUF))
        conn.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SNDBUF)
    if RCVBUF != 0:
        rcvbuf = conn.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF)
        log.debug('Default RCVBUF %s changed to %s' % (rcvbuf, RCVBUF))
        conn.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, RCVBUF)
    
# Authenticate
    conn.sendall(hdr.pack(MsgType.LOGIN, genMsgId(), len(TOKEN)))
    conn.sendall(TOKEN)
    data = receive(conn, hdr.size)
    if not data:
        log.debug("Auth timeout")
        return

    msg_type, msg_id, status = hdr.unpack(data)
    dump("Got {0}, {1}, {2}".format(msg_type, msg_id, status))

    if status != MsgStatus.OK:
        log.debug("Auth failed: %d" % status)
        return

    rt = Thread(target=readthread,  args=(conn,))
    wt = Thread(target=writethread, args=(conn,))
    push = Thread(target=pushMessages, args=(conn,))


    wt.start()
    rt.start()
    push.start()


    push.join()
    wt.join()
    rt.join()
    try:
        log.debug("Attempting to close connection")
        conn.close()
    except:
        log.debug("Failed to close connection, retuning")







