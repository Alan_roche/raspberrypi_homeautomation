import imaplib
import email
import MySQLdb
import NodeInformation
import threading
import time
import NodeInformation
import logging
import socket


Running = True

class GoogleMail(threading.Thread):


    def run(self):

        threading.current_thread().name='Thread - Google Mail Checking'
        print NodeInformation.OKGREEN + "\nThread Name : " + threading.currentThread().getName() + "\n" + NodeInformation.OKWHITE
        self.CheckGmail()



    #connect to gmail
    def CheckGmail(self):

        log = logging.getLogger('GoogleMailCheck')



        while(Running):

            try:
                log.debug("Starting Gmail Check")

                log.debug("Setting Socket Timeout")
                socket.setdefaulttimeout(10)

                log.debug("Connecting SSL to Gmail %s ",NodeInformation.gmail_address)

                mail = imaplib.IMAP4_SSL(NodeInformation.gmail_address)

                log.debug("Connected SSL to Gmail %s ",NodeInformation.gmail_address)

                log.debug("Logging into Gmail")

                mail.login(NodeInformation.gmail_user,NodeInformation.gmail_pass)

                log.debug("Logging into Gmail Completed")

                log.debug("Opening Inbox")
                mail.select('inbox')
                log.debug("Opening Inbox Completed")

                log.debug("Listing Inbox")
                mail.list()
                log.debug("Listing Inbox Completed")


                log.debug("Searching for ALL email")
                typ, data = mail.search(None, 'ALL')
                log.debug("Searching for ALL email Completed")

                for num in data[0].split():
                    log.debug("Starting to fetch new email")

                    log.debug("Searching for ALL email RFC822")
                    typ, data = mail.fetch(num, '(RFC822)')
                    log.debug("Searching for ALL email RFC822 Completed")

                    log.debug("Searching for ALL")
                    typ, data = mail.search(None, 'All')
                    log.debug("Searching for ALL Completed")

                log.debug("Assigning IDS Data")
                ids = data[0]
                id_list = ids.split()
                log.debug("Assigning IDS Data Completed")


                log.debug("Getting Email index ID")
                latest_email_id = int( id_list[-1] )
                log.debug("Getting Email index ID Completed %s ",latest_email_id)
 
                for i in range(latest_email_id, latest_email_id-1, -1):

                    log.debug("Fetch Email with index ID")
                    typ, data = mail.fetch( i, '(RFC822)')
                    log.debug("Fetch Email with index ID Completed")

                for response_part in data:
                    if isinstance(response_part, tuple):
                        log.debug("Parsing Email Details")
                        msg = email.message_from_string(response_part[1])

                        log.debug("Parse Subject")
                        varSubject = msg['subject']
                        log.debug("Parse Subject Completed : %s",varSubject)

                        log.debug("Parse Body")
                        varBody = data[0][1]
                        log.debug("Parse Body Completed : %s",varBody)


                        log.debug("Parse From")
                        varFrom = msg['from']
                        log.debug("Parse From Completed :  %s",varFrom)

                        print "Message Received from : ", varFrom

                        log.debug("Replace <>")
                        EmailStart = varFrom.find('<')
                        EmailStop = varFrom.find('>')
                        log.debug("Replace <> Completed")


                        #print EmailStart
                        #print EmailStop

                        if EmailStart == -1 or EmailStop == -1:
                            log.debug("Not Fixing email from Address")
                            print "Not Fixing Email Address"
                        else:
                            print "Fixing Email Address"
                            log.debug("Fixing Email From address")
                            varFrom = varFrom[EmailStart+1:EmailStop]
                            log.debug("Fixed Email From Address : %s",varFrom)

                        log.debug("Selecting Message for Deletion")
                        mail.store(latest_email_id, '+FLAGS', '\\Deleted')
                        log.debug("Selecting Message for Deleteion Completed")

                        log.debug("Commit Deletion")
                        mail.expunge()
                        log.debug("Commit Deleteiong Completed")

                        if varSubject == None:
                            print "Email Subject is empty"
                            log.debug("Email Subject is empty")
                            varSubject = "N/A"
                        if varBody == None:
                            print "Email Body is empty"
                            log.debug("Email Body is empty")
                            varBody = "N/A"

                        '''print "\n*****************************************\n"
                        print "From : " , varFrom
                        print "Subject : " , varSubject
                        #print "Body : " , varBody
                        print "\n*****************************************\n"'''

                        log.debug("Starting to parse email type")
                        self.getEmailType(varFrom,varSubject , varBody)
                        log.debug("Parse email type Completed")

            except Exception as e:
                log.debug(e)
                log.debug("No new emails to get")
                print NodeInformation.OKGREEN +"No new emails" + NodeInformation.OKWHITE


            try:
                log.debug("Attempting to close connection")
                mail.close()
                mail.logout()
                log.debug("Close connection Completed")

                    #print NodeInformation.OKGREEN + "Closing connection to Gmail" + NodeInformation.OKWHITE

            except:
                log.debug("Error closing conection skipping")
                print NodeInformation.FAIL + "Unable to cleanup connection to Gmail - FAILED" + NodeInformation.OKWHITE
                log.debug("Completed")

            try:
                log.debug("Logging out")
                mail.logout()
                log.debug("Logging out done...")
            except:
                log.debug("Failed to logout")

            print ("Sleeping Gmail")
            log.debug("Sleeping Gmail")
            time.sleep(20)








    def getSQLConnection(self):

        log = logging.getLogger('GoogleMail.getSQLConnection')

        log.debug("Creating Conneting to SQL")

        db = MySQLdb.connect(host=NodeInformation.sql_address, # your host, usually localhost
                        user=NodeInformation.sql_user, # your username
                        passwd=NodeInformation.sql_pass, # your password
                        db=NodeInformation.sql_database) # name of the data base

        log.debug("Connetion to SQL returning")
        return db


    def getEmailUserInformation(self, emailFromAddr):

        log = logging.getLogger("GoogleMail.getEmailUserInformation")

        try:
            db = self.getSQLConnection()

            cur = db.cursor()

            cur.execute("SELECT * FROM Map_EmailAddress WHERE Email = %s",(emailFromAddr,))

            # print all the first cell of all the rows
            for row in cur.fetchall() :
                return row

        except:
            print NodeInformation.FAIL + "Problem getting current user information" + + NodeInformation.OKWHITE
            log.debug("Problem getting current user information")
            try:
                print NodeInformation.OKGREEN + "Closing DB connection" + NodeInformation.OKWHITE
                log.debug("Closing DB Connection")
                db.close()
            except:
                log.debug("Problem closing DB Connection")
                print NodeInformation.OKGREEN + "Problem closing DB connection" + NodeInformation.OKWHITE


    def getEmailType(self,emailFromAddr,emailSubject,emailMessage):

        log = logging.getLogger("GooleMail.getEmailType")

        log.debug("Starting getEmailType")
        User = self.getEmailUserInformation(emailFromAddr)

        if User != None:

            try:

                DBEmailName = User[0]
                DBEmailAddr = User[1]
                DBEmailGeneral = User[2]

                print "Email Name : %s " % DBEmailName
                print "Email Addr : %s" % DBEmailAddr
                print "General Notifications : %s" % DBEmailGeneral
                log.debug("Email Name : %s , Email Address : %s , General Notification : %s", DBEmailName,DBEmailAddr,DBEmailGeneral)

            except:

                print "Failed to collect new email address details"
                log.debug("Failed to collect new email address details")
                return
        else:
            print NodeInformation.WARNING + "Email address not found !! Skipping" + NodeInformation.OKWHITE
            log.debug("Email address not found !, Skipping")

        if emailFromAddr == NodeInformation.gmail_user:
            print NodeInformation.OKGREEN + "Email appears to be a general" + NodeInformation.OKWHITE
            log.debug("Email appears to be for a all general users")

            self.addEmailMessageGeneral(emailSubject, emailMessage)

        elif emailFromAddr == User[1] and not emailFromAddr == NodeInformation.gmail_user:
            print"Email appears to be a private"
            self.addEmailMessagePrivate(emailSubject, emailMessage , User[0])
            log.debug("Email address appears to be a private message")
        else:
            print "Email address unknown"
            log.debug("Email address unknown")

        log.debug("getEmailType Completed")



    def addEmailMessagePrivate(self, emailSubject, emailMessage , User):
        print "Adding PRIVATE Email Message"

        log = logging.getLogger('GoogleMail.addEmailMessagePrivate')

        log.debug("Adding Private Email Message")

        try:
            db = self.getSQLConnection()

            cur = db.cursor()

            try:

                cur.execute("INSERT INTO Messages_Global(Subject,Message,User,isGeneral,isPrivate) VALUES (%s,%s,%s,0,1)" , (MySQLdb.escape_string(emailSubject),MySQLdb.escape_string(emailMessage),User))
                db.commit()

            except:
                print "Error saving database INSERT"
                log.debug("Error Saving Database INSERT")
                try:
                    db.close()
                except:
                    log.debug("Error Closing Database Connection")
                    print "Error closing database connecton"

            db.close()

        except:
            print "Problem getting current user information"
            log.debug("Problem getting current user information")
            try:
                print "Closing DB connection"
                db.close()
            except:
                log.debug("Problem closing DB connection")
                print "Problem closing DB connection"









    def addEmailMessageGeneral(self, emailSubject, emailMessage):
        print "Adding GERNERAL Email Message"

        log = logging.getLogger('GoogleMail.addEmailMessageGeneral')


        log.debug("Adding General Email Message")
        try:
            db = self.getSQLConnection()

            cur = db.cursor()

            cur.execute("SELECT * FROM Map_EmailAddress WHERE GeneralMessages = 1")

            #UserNameActiveGeneral = []

            print "Finding all user registered for general messages"
            log.debug("Finding all user Registered for General Message")

            for row in cur.fetchall():
                #UserNameActiveGeneral.append(row[0])
                print "Adding General User : " , row[0]
                log.debug("Adding General User : %s", row[0])

                try:

                    cur.execute("INSERT INTO Messages_Global(Subject,Message,User,isGeneral,isPrivate) VALUES (%s,%s,%s,1,0)" , (MySQLdb.escape_string(emailSubject),MySQLdb.escape_string(emailMessage),row[0]))
                    db.commit()

                except:
                    print "Error saving database INSERT"
                    log.debug("Error Saving Database INSERT")
                    try:
                        db.close()
                    except:
                        log.debug("Error Closing Database Connetion")
                        print "Error closing database connecton"

            db.close()

        except:
            print "Problem getting current user information"
            log.debug("Problem getting current user information")

            try:
                print "Closing DB connection"
                log.debug("Closing DB Connection")
                db.close()
            except:
                log.debug("Error closing DB Connection")
                print "Problem closing DB connection"

















