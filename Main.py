import CheckDevices
import CheckNetworkStatus
import CheckNotifications
#import UpdateCloudStatus
import GoogleMail
import time
import Logger
import logging
import BlynkClientV2
import NodeInformation
import UpdateNodeStatus
#import CheckNotificationWelcome
import iPhoneCheck
import PlaySound



print("Attemping to int all threads ...")

Logger.startLogger()

log = logging.getLogger('Main')

log.debug("Main Thread started")



try:

    log.debug("Starting CheckDevices Thread")
    y = CheckDevices.CheckDevices()
    y.daemon = True


    log.debug("Starting GoogleMail Thread")
    x = GoogleMail.GoogleMail()
    x.daemon = True


    log.debug("Starting CheckNotifications Thread")
    z = CheckNotifications.CheckNotifications()
    z.daemon = True

    log.debug("Starting BlynkClientStatusUpdate Thread")
    q = BlynkClientV2.BlynkStatus(NodeInformation.blynkToken)
    q.daemon = True


    log.debug("Starting UpdateNoteStatus Thread")
    e = UpdateNodeStatus.UpdateNodeStatus()
    e.daemon = True


    log.debug("Starting iPhone Checker")
    i = iPhoneCheck.iPhoneCheck()
    i.daemon = True

    #log.debug("Starting CheckNotificationWelcome Thread")
    #p = CheckNotificationWelcome.CheckNotificationsWelcome()
    #p.daemon = True


    #Start All Threads
    #y.start()
    log.debug("Started CheckDevices Thread")

    x.start()
    log.debug("Started GoogleMail Thread")

    z.start()
    log.debug("Started CheckNotifications Thread")

    q.start()
    log.debug("Started CloudStatusUpdate Thread")

    e.start()
    log.debug("Started UpdateNoteStatus Thread")

    i.start()
    log.debug("Started iPhoneCheck Thread")

    #p.start()
    #log.debug("Started CheckNotificationsWelcome Thread")


    PlaySound.playSound("StartupSound.mp3")






    '''b = CheckNetworkStatus.CheckNetworkStatus()
    b.daemon=True
    b.start()
    '''





    print("All threads are now running...")

    while(True):
        time.sleep(1)

except KeyboardInterrupt:

    print "Closing Application ... "

except:

    print "Closing Application Fatal Error ..."
