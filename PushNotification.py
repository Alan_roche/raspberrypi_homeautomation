
import httplib
import urllib
import NodeInformation
import logging


def SendPushOver(Message):

        log = logging.getLogger('PushOver.SendPushOver')



        log.debug("Sending PushNotification")

        conn = httplib.HTTPSConnection("api.pushover.net:443")
        conn.request("POST", "/1/messages.json",
          urllib.urlencode({
            "token": NodeInformation.pushoverToken,
            "user": NodeInformation.pushoverUser,
            "message": Message,
          }), { "Content-type": "application/x-www-form-urlencoded" })
        conn.getresponse()

        log.debug("Sending PushNotification - DONE")