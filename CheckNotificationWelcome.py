import threading
import NodeInformation
import MySQLdb
import logging
import datetime
import time


class CheckNotificationsWelcome(threading.Thread):

    def run(self):

        threading.current_thread().name='Thread - Notification Welcome Checking'
        print NodeInformation.OKGREEN + "\nThread Name : " + threading.currentThread().getName() + "\n" + NodeInformation.OKWHITE
        self.CheckNotificationsWelcome()



    def getSQLConnection(self):

        db = MySQLdb.connect(host=NodeInformation.sql_address, # your host, usually localhost
                         user=NodeInformation.sql_user, # your username
                          passwd=NodeInformation.sql_pass, # your password
                          db=NodeInformation.sql_database) # name of the data base
        return db



    def setDeviceNotification(self,Devicename , DeviceBSD , Status):
        db = self.getSQLConnection()

        cur = db.cursor()

        cur.execute("update Global_Device_Table SET Device_Notification=%s , Device_Current_Location = %s WHERE Device_Name = %s" ,(Status,NodeInformation.getNodeName(), Devicename))

        # Commit Database Changes
        db.commit()

        db.close()




    def CheckNotificationsWelcome(self):
        log = logging.getLogger('CheckNotificationsWelcome')

        log.debug("CheckNotificationsWelcome Starting")


        while True:
            db = self.getSQLConnection()

            cur = db.cursor()

            cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Notification = 0")

            for row in cur.fetchall():


                log.debug("Checking date and time for welcome message")
                try:
                    if row[8] < datetime.datetime.now() - datetime.timedelta(minutes=NodeInformation.WelcomeMessageAlert):
                        log.debug ("Update Welcome Message REQUIRED")
                        self.setDeviceNotification(row[0],row[1],1)
                    else:
                        log.debug ("Update Welcome Message NOT REQUIRED")
                except:
                    log.debug ("Problem setting Welcome Message details skipping table")
                    log.debug(row)
            print "Welcome Message Check Finished"
            time.sleep(60)








