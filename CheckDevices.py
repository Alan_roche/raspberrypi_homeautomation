import BluetoothConnect
import DeviceDBupdate
import NodeInformation
import logging
import threading
import time

class CheckDevices(threading.Thread):



    def run(self):

        log = logging.getLogger('CheckDevices')
        log.debug("CheckDevices Starting")


        threading.current_thread().name='Thread - Bluetooth Device Checking'

        print NodeInformation.OKGREEN + "\nThread Name : " + threading.currentThread().getName() + "\n" + NodeInformation.OKWHITE


        while True:

            print NodeInformation.OKGREEN + "\n-----------Starting to collect data from SQL Server-----------\n" + NodeInformation.OKWHITE

            log.debug("Starting to collect data from SQL Server")
            DeviceNames , DeviceBSD = DeviceDBupdate.getDevices()
            log.debug("Starting to collect data from SQL Server Completed")

            print NodeInformation.OKGREEN + "\n-----------DONE-----------\n" + NodeInformation.OKWHITE

            for (deviceBSD, devicename) in zip(DeviceBSD , DeviceNames):

                log.debug("Processing Device Status")
                Status , Device_RSSI = BluetoothConnect.poll_device(deviceBSD , devicename)


                if Status:
                    DeviceDBupdate.UpdateDeviceStatus(devicename,deviceBSD,1 , Device_RSSI)


                else:

                    DeviceDBupdate.UpdateDeviceStatus(devicename,deviceBSD ,0 , Device_RSSI)

                log.debug("Processing Device Status Completed")
