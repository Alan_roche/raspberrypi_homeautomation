import threading
import MySQLdb
import NodeInformation
import time
import datetime
import logging





class UpdateNodeStatus(threading.Thread):
    Running = True



    def getSQLConnection(self):

                db = MySQLdb.connect(host=NodeInformation.sql_address, # your host, usually localhost
                                user=NodeInformation.sql_user, # your username
                                passwd=NodeInformation.sql_pass, # your password
                                db=NodeInformation.sql_database) # name of the data base
                return db

    def run(self):

        log = logging.getLogger('UpdateNodeStatus')



        log.debug("Getting Update interval")
        db = self.getSQLConnection()
        cur = db.cursor()
        cur.execute("SELECT * FROM Global_Node_Status WHERE NodeName = %s", (NodeInformation.getNodeName(),))

        result = cur.fetchall()


        try:
            log.debug(result)
            UpdateSchedule = result[0][2]
            log.debug("Update Schedule : %s" , UpdateSchedule)
        except Exception as e:
            log.debug("Failed to update schedule from DB setting default 60")
            log.debug(e)
            UpdateSchedule = 60





        while self.Running:

            db = self.getSQLConnection()
            cur = db.cursor()

            log.debug("Updating Node Online Status")

            cur.execute("UPDATE Global_Node_Status SET NodeName=%s, LastActive =%s WHERE NodeName = %s",(NodeInformation.getNodeName(),datetime.datetime.now(),NodeInformation.getNodeName()))

            db.commit()
            log.debug("Commited DB Changes")
            db.close()
            log.debug("Closed DB Connection")
            time.sleep(UpdateSchedule)















