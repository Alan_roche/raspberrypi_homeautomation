import select, socket, struct
import os, sys, time, getopt ,logging , NodeInformation ,MySQLdb
from threading import Thread

class MsgType:
    RSP    = 0
    LOGIN  = 2
    PING   = 6
    BRIDGE = 15
    HW     = 20

class MsgStatus:
    OK     = 200











class BlynkStatus(Thread):

    hdr = struct.Struct("!BHH")

    SERVER = "cloud.blynk.cc"
    PORT = 8442
    NODELAY = 1     # TCP_NODELAY
    SNDBUF = 0      # No SNDBUF override
    RCVBUF = 0      # No RCVBUF override
    TOKEN = "YourAuthToken"
    DUMP = 0

    static_msg_id = 1

    Connected = True
    ThreadWrite = True
    ThreadRead = True
    ThreadPush = True



    def __init__(self,Token):
        Thread.__init__(self)
        log = logging.getLogger('BlynkClient.Blnkstatus')
        self.TOKEN = Token

        log.debug(("Token set to : " + Token))

    def genMsgId(self):

        if self.static_msg_id > 3000:
            print ("**** RESETTING STATIC_MSG_ID")
            self.static_msg_id = 1

        self.static_msg_id += 1
        return self.static_msg_id

    # Other utilities

    start_time = time.time()

    def log(self,msg):
        print "[{:7.3f}] {:}".format(float(time.time() - self.start_time), msg)

    def dump(self,msg):
        if self.DUMP:
            self.log(msg)

    def receive(self,sock, length):
        d = []
        l = 0
        while l < length and self.Connected:
            r = ''
            try:
                r = sock.recv(length-l)
            except socket.timeout:
                continue
            if not r:
                return ''
            d.append(r)
            l += len(r)
        return ''.join(d)





    def hw(self,*args):
        # Convert params to string and join using \0
        data = "\0".join(map(str, args))
        self.dump("< " + " ".join(map(str, args)))
        # Prepend HW command header
        return self.hdr.pack(MsgType.HW, self.genMsgId(), len(data)) + data


    def handle_hw(self,data,conn):

        log = logging.getLogger('BlynkClient.handle_hw')

        params = data.split("\0")
        cmd = params.pop(0)
        if cmd == 'info':
            pass

        ### DIRECT pin operations
        elif cmd == 'pm':
            pairs = zip(params[0::2], params[1::2])
            for (pin, mode) in pairs:
                pin = int(pin)
                if mode == 'in':
                    log.debug("Pin %d mode INPUT" % pin)
                elif mode == 'out':
                    log.debug("Pin %d mode OUTPUT" % pin)
                elif mode == 'pu':
                    log.debug("Pin %d mode INPUT_PULLUP" % pin)
                elif mode == 'pd':
                    log.debug("Pin %d mode INPUT_PULLDOWN" % pin)
                else:
                    log.debug("Unknown pin %d mode: %s" % (pin, mode))

        elif cmd == 'dw':
            pin = int(params.pop(0))
            val = params.pop(0)
            log.debug("Digital write pin %d, value %s" % (pin, val))

        elif cmd == 'aw':
            pin = int(params.pop(0))
            val = params.pop(0)
            log.debug("Analog write pin %d, value %s" % (pin, val))

        elif cmd == 'dr':           # This should read digital pin
            pin = int(params.pop(0))
            log.debug("Digital read pin %d" % pin)
            conn.sendall(self.hw("dw", pin, 1)) # Send value

        elif cmd == 'ar':           # This should do ADC read
            pin = int(params.pop(0))
            log.debug("Analog read pin %d" % pin)
            conn.sendall(self.hw("aw", pin, 123)) # Send value

        ### VIRTUAL pin operations
        elif cmd == 'vw':           # This should call user handler
            pin = int(params.pop(0))
            val = params.pop(0)
            log.debug("Virtual write pin %d, value %s" % (pin, val))


            if (pin == 50):

                if val == "1":
                    log.debug("BUTTON ON")
                if val == "0":
                    log.debug("BUTTON OFF")



        elif cmd == 'vr':           # This should call user handler
            pin = int(params.pop(0))
            log.debug("Virtual read pin %d" % pin)

            log.debug("Virtual read pin %d" % pin)

            if pin == 0:
                self.updateTotalNotifications(conn,pin)
                log.debug("Virtual PIN 0 Found updating email status")

            if pin == 1:
                self.getTotalClientsOnline(conn,pin)

            if pin == 2:
                self.getTotalClientsOffline(conn,pin)

        else:
            log.debug("Unknown HW cmd: %s" % cmd)


    def readthread(self,conn):

        log = logging.getLogger('BlynkClient.readthread')


        try:
            while (self.Connected):

                self.ThreadRead = True

                data = self.receive(conn, self.hdr.size)
                if not data:
                    break
                msg_type, msg_id, msg_len = self.hdr.unpack(data)
                self.dump("Got {0}, {1}, {2}".format(msg_type, msg_id, msg_len))
                if msg_type == MsgType.RSP:
                    pass
                elif msg_type == MsgType.PING:
                    log.debug("Got ping")
                    # Send Pong
                    conn.sendall(self.hdr.pack(MsgType.RSP, msg_id, MsgStatus.OK))
                elif msg_type == MsgType.HW or msg_type == MsgType.BRIDGE:
                    data = self.receive(conn, msg_len)
                    # Print HW message
                    self.dump("> " + " ".join(data.split("\0")))
                    self.handle_hw(data,conn)

                    self.pushMessages(conn)

                else:
                    log.debug("Unknown msg type")
                    continue
            self.ThreadWrite = False
            self.ThreadRead = False
            self.Connected = False
            log.debug  ("Thread to CLOSING : READ-THREAD")

        except:
            self.ThreadWrite = False
            self.ThreadRead = False
            self.Connected = False
            log.debug  ("FORCING Thread to CLOSE : READ-THREAD")


    def writethread(self,conn):

        log = logging.getLogger('BlynkClient.writethread')


        try:
            while (self.Connected):

                self.ThreadWrite = True

                time.sleep(5)
                log.debug("Sending heartbeat...")
                conn.sendall(self.hdr.pack(MsgType.PING, self.genMsgId(), 0))
                #Send Bar chart updates
                conn.sendall(self.hw("vr", 31, 100))
                conn.sendall(self.hw("vr", 31, 1))


            self.ThreadWrite = False
            self.ThreadRead = False
            self.Connected = False
            log.debug("Thread to CLOSING : WRITE-THREAD")

        except:
            self.ThreadWrite = False
            self.ThreadRead = False
            self.Connected = False
            log.debug("FORCING Thread to CLOSE : WRITE-THREAD")

    def onLinkCheck(self):

        log = logging.getLogger('BlynkClient.onlineCheck')


        try:
            while (self.Connected):
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                result = sock.connect_ex((self.SERVER,self.PORT))
                if result == 0:
                    log.debug("Connected")
                    #self.Connected = True
                else:
                    log.debug("Not Connect")
                    self.Connected = False

                log.debug('Closing socket')
                sock.close()
                log.debug('Closing socket - DONE')


                time.sleep(5)
        except:
            log.debug ("Error in onLineCheck")
            self.Connected = False




    def getSQLConnection(self):

            db = MySQLdb.connect(host=NodeInformation.sql_address, # your host, usually localhost
                            user=NodeInformation.sql_user, # your username
                            passwd=NodeInformation.sql_pass, # your password
                            db=NodeInformation.sql_database) # name of the data base
            return db


    def updateTotalNotifications(self,conn ,pin):
        db = self.getSQLConnection()
        cur = db.cursor()
        cur.execute("SELECT * FROM Messages_Global")
        sqlMsgCount = len(cur.fetchall())
        conn.sendall(self.hw("vw", pin, sqlMsgCount))
        db.close()

    def getTotalClientsOnline(self,conn,pin):
        db = self.getSQLConnection()
        cur = db.cursor()
        cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Status = '1' and Device_Current_Location = %s", (NodeInformation.getNodeName(),))
        sqlOnlineClients = len(cur.fetchall())
        conn.sendall(self.hw("vw", pin, sqlOnlineClients))
        db.close()

    def getTotalClientsOffline(self,conn, pin):
        db = self.getSQLConnection()
        cur = db.cursor()
        cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Status = '0' and Device_Current_Location = %s", (NodeInformation.getNodeName(),))
        sqlOfflineClients = len(cur.fetchall())
        conn.sendall(self.hw("vw", pin, sqlOfflineClients))
        db.close()


    def processCommands(self,conn,pin):
        log = logging.getLogger('BlynkClient.processComands')

        log.debug("Process Command DEF")
        if pin == 50:
            log.debug("Running Reboot")
            os.system("reboot")


    def pushMessages(self,conn):
        log = logging.getLogger('BlynkClient.pushMessages')

        status = 0

        try:
            #while (self.Connected and self.ThreadWrite and self.ThreadRead):

            if status == 1:
                status = 0
            else:
                status = 1

            log.debug("pushMessages to Cloud")
            conn.sendall(self.hw("vr", 30, status))
                #time.sleep(1)

            log.debug("Thread to CLOSING : PUSHMessages - THREAD")
        except:
            self.Connected = False
            log.debug("FORCING Thread to CLOSE : PUSHMessages - THREAD")

    def resetConnection(self):
        self.Connected = True
        self.ThreadRead = True
        self.ThreadWrite = True









    def run(self):

        log = logging.getLogger('BlynkClient.run')


        while True:



            self.resetConnection()

            log.debug('Connecting to %s:%d' % (self.SERVER, self.PORT))
            try:
                conn = socket.create_connection((self.SERVER, self.PORT), 3)
            except Exception as e:
                log.debug("Can't connect")
                log.debug(e)
                continue

            log.debug('Socket Connected, Moving to NODELAY')

            try:
                if self.NODELAY != 0:
                    log.debug('Starting NODELAY if statement')
                    conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
                    log.debug('Finished NODELAY if statement')
                if self.SNDBUF != 0:
                    log.debug('Starting SENDBUF if statement')
                    sndbuf = conn.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF)
                    log.debug('Default SNDBUF %s changed to %s' % (sndbuf, self.SNDBUF))
                    conn.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, self.SNDBUF)
                if self.RCVBUF != 0:
                    log.debug('Starting RCVBUF if statement')
                    rcvbuf = conn.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF)
                    log.debug('Default RCVBUF %s changed to %s' % (rcvbuf, self.RCVBUF))
                    conn.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, self.RCVBUF)
            except:
                log.debug('ERROR - Problem with if statement loop')

            try:
                log.debug('Starting Authentication Check')
                # Authenticate
                conn.sendall(self.hdr.pack(MsgType.LOGIN, self.genMsgId(), len(self.TOKEN)))
                log.debug('Sendall Completed')
                conn.sendall(self.TOKEN)
                log.debug('Sending Token')
                data = self.receive(conn, self.hdr.size)
                log.debug('Sending conn, self.hdr.Size')
                if not data:
                    log.debug("Auth timeout")
                    continue
                log.debug('Starting MSG Unpack')
                msg_type, msg_id, status = self.hdr.unpack(data)
                log.debug('Finished msg_type , msg_id')
                self.dump("Got {0}, {1}, {2}".format(msg_type, msg_id, status))
                log.debug('Finsihed self.dump')

                log.debug('Message Status')
                log.debug(status)
                if status != MsgStatus.OK:
                    log.debug("Auth failed: %d" % status)
                    continue
            except Exception as e:
                log.debug("ERROR - Auth Failed")
                log.debug(e)
                continue

            self.Connected = True

            linkcheck = Thread(target=self.onLinkCheck,)

            wt = Thread(target=self.readthread,  args=(conn,))
            rt = Thread(target=self.writethread, args=(conn,))
            push = Thread(target=self.pushMessages, args=(conn,))

            #push.start()
            wt.start()
            rt.start()
            #linkcheck.start()


            #linkcheck.join()
            wt.join()
            rt.join()

            try:
                conn.close()
            except:
                log.debug ("Failed to close connection..")

            log.debug("Waiting 10 seconds for reconnection...")

            time.sleep(10)












