import threading
import NodeInformation


class CheckNetworkStatus(threading.Thread):

    def run(self):

        threading.current_thread().name='Thread - System Status Checking'
        print NodeInformation.OKGREEN + "\nThread Name : " + threading.currentThread().getName() + "\n" + NodeInformation.OKWHITE
        self.startDeviceCheck()



    def startDeviceCheck(self):
        print "Starting Device Check"