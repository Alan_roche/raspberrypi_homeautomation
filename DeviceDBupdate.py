import MySQLdb
import NodeInformation
import SpeakText
import logging
import datetime
import BluetoothConnect



def getSQLConnection():

    db = MySQLdb.connect(host=NodeInformation.sql_address, # your host, usually localhost
                     user=NodeInformation.sql_user, # your username
                      passwd=NodeInformation.sql_pass, # your password
                      db=NodeInformation.sql_database) # name of the data base
    return db




def getDevices():
    db = getSQLConnection()

    cur = db.cursor()

    #cur.execute("SELECT * FROM Global_Device_Table WHERE `Device_Name` = 'Alan Roche'")
    print NodeInformation.OKGREEN + "\n---------------Collect Active Devices---------------------" + NodeInformation.OKWHITE

    cur.execute("SELECT * FROM Global_Device_Table")

    list_Name = []
    list_Devices = []

# print all the first cell of all the rows
    for row in cur.fetchall() :
        list_Name.append(row[0])
        list_Devices.append(row[1])

        print "Device Added : " , row[0]
        print "Device BSD   : " , row[1]
        print "Device Online: " , row[5]

    print NodeInformation.OKGREEN + "---------------------DONE---------------------------\n"+ NodeInformation.OKWHITE

    return list_Name , list_Devices





def UpdateDeviceStatus(Devicename, DeviceBSD, Status , Device_RSSI):

    log = logging.getLogger('DeviceDBupdate')


    log.debug("Starting DeviceDBupdate")

    db = getSQLConnection()

    cur = db.cursor()

    cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Name = %s AND Device_ID = %s" , (Devicename , DeviceBSD))

    device = []

    for row in cur.fetchall():
        device.append(row[0])
        device.append(row[1])
        device.append(row[2])
        device.append(row[3])
        device.append(row[4])
        device.append(row[5])
        device.append(row[6])
        device.append(row[7])
        device.append(row[8])

    db.close()


    '''
    print "\n----------------------------------------------------------"

    print "Device [0] : " ,device[0]
    print "Device [1] : " ,device[1]
    print "Device [2] : " ,device[2]
    print "Device [3] : " ,device[3]
    print "Device [4] : " ,device[4]
    print "Device [4] : " ,device[5]
    print "Device [4] : " ,device[6]

    print "----------------------------------------------------------\n"
    '''




    print NodeInformation.OKGREEN + "\n-----------------Update Device Status----------------------\n" + NodeInformation.OKWHITE

    print NodeInformation.OKGREEN + "\nUpdate Welcome Messsage Status for all currently conencted devices\n" + NodeInformation.OKWHITE

    CheckNotificationsWelcome()

    print NodeInformation.OKGREEN + "\nUpdate Welcome Messsage Status for all currently conencted devices - DONE\n" + NodeInformation.OKWHITE



    if device[0] == Devicename and device[1] == DeviceBSD and device[4] == NodeInformation.getNodeName() and Status == 1:

        '''if device[6] == 1:
            SpeakText.GoogleSpeakText("Welcome back %s " % device[2])
            setDeviceNotification(device[0] , device[1] , 0)
            log.debug("Welcome Message spoke")
        '''


        print NodeInformation.OKBLUE + "Device %s Online already connected to current Node : %s" % (Devicename,  device[4]) +NodeInformation.OKWHITE
        setDeviceLocation(device[0] , device[1] , NodeInformation.getNodeName(), device[3], 1 , Device_RSSI, datetime.datetime.now())

        log.debug("Device %s Online already connected to current Node : %s" % (Devicename,  device[4]))

        # setDeviceLocation(device[0] , device[1] , DeviceCurrentLocation , DeviceLastLocation)

    if device[0] == Devicename and device[1] == DeviceBSD and not device[4] == NodeInformation.getNodeName() and Status == 1:

        '''if device[6] == 1:
            SpeakText.GoogleSpeakText("Welcome back %s "% device[2])
            setDeviceNotification(device[0] , device[1] , 0)
        '''

        print NodeInformation.OKBLUE + "Device %s Online connecting to current Node : %s" % (Devicename,  NodeInformation.getNodeName()) + NodeInformation.OKWHITE
        setDeviceLocation(device[0] , device[1] , NodeInformation.getNodeName(), device[4], 1 , Device_RSSI, datetime.datetime.now())
        log.debug("Device %s Online connecting to current Node : %s" % (Devicename,  NodeInformation.getNodeName()))


    if device[0] == Devicename and device[1] == DeviceBSD and device[4] == NodeInformation.getNodeName() and Status == 0:
        print NodeInformation.FAIL +  "Device %s Offline and connected to this Node : %s " % (Devicename, NodeInformation.getNodeName()) + NodeInformation.OKWHITE
        setDeviceLocation(device[0] , device[1] , NodeInformation.getNodeName(), device[4], 0 , Device_RSSI ,device[8])

        #setDeviceNotification(device[0] , device[1] , 1)

        log.debug("Device %s Offline and connected to this Node : %s " % (Devicename, NodeInformation.getNodeName()))


    if device[0] == Devicename and device[1] == DeviceBSD and not device[4] == NodeInformation.getNodeName() and Status == 0:
        print NodeInformation.WARNING + "Device %s Offline and not connected to this Node : %s Current Node : %s" % (Devicename,NodeInformation.getNodeName(), device[4]) + NodeInformation.OKWHITE
        log.debug("Device %s Offline and not connected to this Node : %s Current Node : %s" % (Devicename,NodeInformation.getNodeName(), device[4]))

    print NodeInformation.OKGREEN + "\n----------------------DONE---------------------------\n" +NodeInformation.OKWHITE




def setDeviceLocation(Devicename , DeviceBSD , DeviceCurrentLocation , DeviceLastLocation , DeviceStatus ,Device_RSSI , Device_LastConnected):

    #Fix issue where new device might not have a last connected status
    if Device_LastConnected == None:
        Device_LastConnected = datetime.datetime.now()

    db = getSQLConnection()

    cur = db.cursor()

# Use all the SQL you like
    cur.execute("update Global_Device_Table SET Device_Current_Location=%s , Device_Last_Location = %s , Device_Status = %s , Device_RSSI = %s , Device_LastConnected = %s WHERE Device_Name = %s AND Device_ID = %s" ,(DeviceCurrentLocation, DeviceLastLocation, DeviceStatus, Device_RSSI, Device_LastConnected, Devicename , DeviceBSD))

    # Commit Database Changes
    db.commit()
    db.close()



def setDeviceOnline(Devicename , DeviceBSD , Status):
    db = getSQLConnection()

    cur = db.cursor()

    # Use all the SQL you like
    cur.execute("update Global_Device_Table SET Device_Status=%s , Device_Current_Location = %s WHERE Device_Name = %s" ,(Status,NodeInformation.getNodeName(), Devicename))

    # Commit Database Changes
    db.commit()
    db.close()





def setDeviceNotification(Devicename , DeviceBSD , Status):
    db = getSQLConnection()

    cur = db.cursor()

    cur.execute("update Global_Device_Table SET Device_Notification=%s , Device_Current_Location = %s WHERE Device_Name = %s" ,(Status,NodeInformation.getNodeName(), Devicename))

    # Commit Database Changes
    db.commit()

    db.close()


def CheckNotificationsWelcome():
    log = logging.getLogger('CheckNotificationsWelcome')

    log.debug("CheckNotificationsWelcome Starting")


    db = getSQLConnection()

    cur = db.cursor()

    cur.execute("SELECT * FROM Global_Device_Table WHERE Device_Notification = 0 AND Device_Current_Location = %s",(NodeInformation.getNodeName(),) )

    for row in cur.fetchall():

        log.debug("Checking date and time for welcome message")
        try:
            if row[8] < datetime.datetime.now() - datetime.timedelta(minutes=NodeInformation.WelcomeMessageAlert):
                log.debug ("Update Welcome Message REQUIRED")
                setDeviceNotification(row[0],row[1],1)
            else:
                log.debug ("Update Welcome Message NOT REQUIRED")

        except:
            log.debug ("Problem setting Welcome Message details skipping table")
            log.debug(row)
    try:
        db.close()
    except:
        log.debug("Failed to close database connection")
    print "Welcome Message Check Finished"
